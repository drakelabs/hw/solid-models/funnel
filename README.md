# Solid Models: Cable hanger

Funnel, parameterizable; designed and built with [OpenSCAD](https://github.com/openscad/openscad). Ready for slicing and printing.

## License

The source code for this model is made available under the GNU Public License v3.
