// SMD Funnel
hole_r = 2.5;
max_r = 30;
h = 30;
wall_thickness = 2;

module funnel() {
    difference() {
        cylinder(r1=max_r, r2=hole_r, h=h, center=true);
        cylinder(r1=max_r-wall_thickness,
            r2=hole_r-wall_thickness,
            h=h,
            center=true);
    };

    difference() {
        cube([
            max_r*2,
            max_r*2,
            h],
            center=true);
        
        union() {
            cube([
                max_r*2 + (wall_thickness*2),
                max_r*2 - (wall_thickness*2),
                h - wall_thickness*2],
                center=true);

            cube([
                max_r*2 - (wall_thickness*2),
                max_r*2 + (wall_thickness*2),
                h - wall_thickness*2],
                center=true);

            //translate([0,0,wall_thickness*2])
            cube([
                max_r*2 - (wall_thickness*2),
                max_r*2 - (wall_thickness*2),
                h + wall_thickness*2],
                center=true);
        }
    };
}

funnel();